# Bicycle Alarm Holder with magnetic lock

![](pictures/DSC_6310.jpg)

**What is it?**
Bicycle alarm holder that mounts on the bicycle frame.
Design is parametric and frame size values can be adjusted within the file.
There are no visible disassembly points as zip ties, bolts and nuts are hidden inside.
It has magnetic lock.

![](pictures/DSC_6458.jpg)

**Why make it?**
[Bicycle Alarm](https://www.amazon.ca/gp/product/B07QYSWSPW/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1) comes with double sided tape and zip ties. Double sided tape seems to be too easy to remove it and zip ties also are not ideal as they are visible and easy to cut off. Also finding a good location for mounting can be tricky.

**Requirements**
Mounts to any point of the frame.
It's not obviously visible how to remove it.

**Effort**
It took 3 iterations. In every design improvements were about making it slimmer and tighter tolerances. Making sure it's easier to print.

CAD time - 2h for each revision


**How does it work?**

There are 3 pieces.
Top - It goes around the frame and covers all other pieces, therefore it gives more uniformal look.
Bottom - This is where alarm gets attached and this piece also used to clamp around the frame. It has a cutout for the magnet to be inserted.
Cover - Once it slides in it covers the bolt heads so they cannot be unscrew unless cover is removed but cover gets locked in place with magnet.

Cover piece has a hook at the end.
![](pictures/Selection_605.png)
![](pictures/Selection_607.png)

Bottom piece has slot for magnet that once magnet is inserted the cover piece hooks around the magnet. Magnet can slide up and down to lock and unlock.

![](pictures/Selection_606.png)
![](pictures/Selection_608.png)

In order to lock/unlock you need to use magnet externally next to the wall and move it up and down. Depending on tolerances if it's loose you can hear magnet get to its limiting position. You will need to have this magnet available as alarm batteries will need changing eventually.

**BOM**

![](pictures/DSC_6283.jpg)

1x Top
1x Bottom
1x Cover
4x M4 Nuts
4x M3x20 Socket Head bolts
1x Magnet 20x3x2mm

Also have some magnets for later in order to lock/unlock.

**Assembly**

Modify spreadsheet values to your frame requirements. Measure Height and Width at the front location and back. Some frames change size as they go. For example it's thicker at the front and going towards the seat it gets smaller.

* Print
* Sand, Paint
* Insert Nuts into the top piece
![](pictures/DSC_6288.jpg)
* Attach alarm with zip ties
![](pictures/DSC_6291.jpg)
![](pictures/DSC_6292.jpg)
![](pictures/DSC_6296.jpg)
* Insert magnet into the slot
![](pictures/DSC_6299.jpg)
* Insert bolts
![](pictures/DSC_6308.jpg)
* Mount top and bottom pieces around the frame
![](pictures/DSC_6301.jpg)
* Insert the cover piece
* Lock it with a magnet
![](pictures/DSC_6398.jpg)
